package de.szut.lf8_project.hello;

import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.hello.dto.HelloCreateDto;
import de.szut.lf8_project.hello.dto.HelloGetDto;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.repository.ProjectRepo;
import de.szut.lf8_project.service.EmployeeService;
import de.szut.lf8_project.service.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "hello")
public class HelloController {
    private final HelloService service;
    private final HelloMapper helloMapper;
    private final ProjectRepo project_repo;

    public HelloController(HelloService service, HelloMapper mappingService, ProjectRepo project_repo) {
        this.service = service;
        this.helloMapper = mappingService;
        this.project_repo = project_repo;
    }

    @Operation(summary = "creates a new hello with its id and message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created hello",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HelloGetDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping
    public HelloGetDto create(@RequestHeader Map<String,String> headers, @RequestBody @Valid HelloCreateDto helloCreateDto) {
        String token = headers.get("token");
        HelloEntity helloEntity = this.helloMapper.mapCreateDtoToEntity(helloCreateDto);
        helloEntity = this.service.create(helloEntity);
        return this.helloMapper.mapToGetDto(helloEntity);
    }

    @Operation(summary = "delivers a list of hellos")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of hellos",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HelloGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping
    public List<HelloGetDto> findAll(@RequestHeader Map<String,String> headers) {
        String token = headers.get("token");
        return this.service
                .readAll()
                .stream()
                .map(e -> this.helloMapper.mapToGetDto(e))
                .collect(Collectors.toList());
    }

    @Operation(summary = "deletes a Hello by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "delete successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteHelloById(@RequestHeader Map<String,String> headers, @RequestParam long id) {
        String token = headers.get("token");
        var entity = this.service.readById(id);
        if (entity == null) {
            throw new ResourceNotFoundException("HelloEntity not found on id = " + id);
        } else {
            this.service.delete(entity);
        }
    }

    @Operation(summary = "find hellos by message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of hellos who have the given message",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HelloGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "qualification description does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/findByMessage")
    public List<HelloGetDto> findAllEmployeesByQualification(@RequestHeader Map<String,String> headers, @RequestParam String message) {
        String token = headers.get("token");
        return this.service
                .findByMessage(message)
                .stream()
                .map(e -> this.helloMapper.mapToGetDto(e))
                .collect(Collectors.toList());
    }
}
