package de.szut.lf8_project.repository;

import de.szut.lf8_project.model.EmployeeQualification;
import de.szut.lf8_project.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeQualificationRepository extends JpaRepository<EmployeeQualification, Long> {
    List<Project> findAllByOrderByIdDesc();
}