package de.szut.lf8_project.repository;

import de.szut.lf8_project.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepo extends JpaRepository<Project, Long> {
    List<Project> findAllByOrderByIdDesc();
}
