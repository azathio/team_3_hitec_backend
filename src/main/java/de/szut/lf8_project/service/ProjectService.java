package de.szut.lf8_project.service;

import de.szut.lf8_project.exceptionHandling.DataIncompleteException;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.model.EmployeeQualification;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.repository.EmployeeQualificationRepository;
import de.szut.lf8_project.repository.ProjectRepo;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ProjectService {
    private final ProjectRepo repository;
    private final EmployeeQualificationRepository employeeQualificationRepository;

    public ProjectService(ProjectRepo repository, EmployeeQualificationRepository employeeQualificationRepository) {
        this.repository = repository;
        this.employeeQualificationRepository = employeeQualificationRepository;
    }

    public Project create(Project newProject) {
        EmployeeQualificationService temp_service = new EmployeeQualificationService(employeeQualificationRepository);
        List<EmployeeQualification> temp_list = newProject.getContributingEmployees();
        for (EmployeeQualification temp_obj:
             temp_list) {
            temp_service.create(temp_obj);
        }
        return repository.save(newProject);
    }

    public List<Project> readAll(){
        return repository.findAllByOrderByIdDesc();
    }

    public Project readById(Long id){
        Optional<Project> s = repository.findById(id);

        if (s.isEmpty()) {
            throw new ResourceNotFoundException("Project with Id: " + id + " could not be found");
        }
        return s.get();
    }

    public Project update(Project newProject, Long id) {
        Project updatedProject = readById(id);
        EmployeeQualificationService temp_service = new EmployeeQualificationService(employeeQualificationRepository);

        updatedProject.setProjectName(newProject.getProjectName());
        updatedProject.setLeadEmployeeId(newProject.getLeadEmployeeId());
        updatedProject.setCustomerId(newProject.getCustomerId());
        updatedProject.setContactPersonName(newProject.getContactPersonName());
        updatedProject.setProjectGoal(newProject.getProjectGoal());
        updatedProject.setStartDate(newProject.getStartDate());
        updatedProject.setPlannedEndDate(newProject.getPlannedEndDate());
        updatedProject.setActualEndDate(newProject.getActualEndDate());
        List<EmployeeQualification> temp_list = newProject.getContributingEmployees();
        for (EmployeeQualification temp_obj:
                temp_list) {
            temp_service.create(temp_obj);
        }
        updatedProject.setContributingEmployees(newProject.getContributingEmployees());

        updatedProject = this.repository.save(updatedProject);
        return updatedProject;
    }

    public Project patch(Map<String,Object> patchData, Long id)
    {
        Project projectToUpdate = readById(id);

        patchData.forEach((key, value) -> {
            Field field = ReflectionUtils.findField(Project.class, key);

            if(field == null)
                throw new DataIncompleteException("Field with name '" + key + "' does not exist");

            field.setAccessible(true);
            ReflectionUtils.setField(field, projectToUpdate, value);
        });

        return this.repository.save(projectToUpdate);
    }
    public void addContributingEmployee(Long projectId, EmployeeQualification employee){
        Optional<Project> s = this.repository.findById(projectId);
        if (s.isPresent()) {
            Project project = s.get();
            List<EmployeeQualification> emp_list = new ArrayList<>();
            emp_list.add(employee);
            List<EmployeeQualification> proj_emp_list = project.getContributingEmployees();
            for (EmployeeQualification emp_obj:
                    proj_emp_list) {
                emp_list.add(emp_obj);
            }
            project.setContributingEmployees(emp_list);
            update(project, project.getId());
        }
    }

    public void delete(Long id){
        if(this.readById(id) == null)
            throw new ResourceNotFoundException("Project with Id: " + id + " could not be found");

        repository.deleteById(id);
    }

    // Checks if the employee is contributing to the project
    public boolean checkIfEmployeeIsContributor(Long projectId, Long employeeId) {
        Project selectedProject = this.readById(projectId);
        if(selectedProject == null)
            return false;

        for (EmployeeQualification emp:
                selectedProject.getContributingEmployees()) {
            if(emp.getEmployeeId() == employeeId){
                return true;
            }
        }

        return false;
    }

    // Removes the employee from the project if he is a contributor
    // Returns boolean if the list was changed
    public boolean removeContributorFromProject(Long projectId, Long employeeId) {
        Project selectedProject = this.readById(projectId);
        if(selectedProject == null)
            return false;
        EmployeeQualification emp = new EmployeeQualification();
        List<EmployeeQualification> newContributerList = null;
        newContributerList = selectedProject.getContributingEmployees();
        for (EmployeeQualification quali:
                newContributerList) {
            if(quali.getEmployeeId() == employeeId){
                emp = quali;
            }
        }
        boolean listChangedResult = newContributerList.remove(emp);

        if(listChangedResult) {
            selectedProject.setContributingEmployees(newContributerList);
            this.repository.save(selectedProject);
        }

        return listChangedResult;
    }
}
