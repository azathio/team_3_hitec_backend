package de.szut.lf8_project.service;

import de.szut.lf8_project.model.EmployeeQualification;
import de.szut.lf8_project.model.dto.EmployeeDTO;
import de.szut.lf8_project.model.dto.EmployeeQualificationsDTO;
import de.szut.lf8_project.model.dto.QualificationsDTO;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.templates.EmployeeAccessTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class EmployeeService {
    EmployeeAccessTemplate employeeAccess;
    public EmployeeService(){
        employeeAccess = new EmployeeAccessTemplate();
    }

    public EmployeeDTO getEmployeeById(long employeeId, String token) {
        return employeeAccess.getEmployeeByUrlAndId("employees/" + employeeId, token);
    }

    public boolean checkIfEmployeeExists(long employeeId, String token){
        boolean retour = true;
        EmployeeDTO employeeFromAPI = this.getEmployeeById(employeeId, token);
        if(employeeFromAPI == null){
            retour = false;
        }

        return retour;
    }

    public boolean checkIfEmployeeOwnsQualification(long employeeId, String qualification, String token){
        boolean retour = false;
        EmployeeQualificationsDTO employeeFromAPI = new EmployeeQualificationsDTO();
        employeeFromAPI = employeeAccess.getEmployeeWithQualificationsByUrlAndId("employees/" + employeeId + "/qualifications", token);
        if(employeeFromAPI == null){
            return retour;
        }
        else {
            for (QualificationsDTO qualifications:
                 employeeFromAPI.getSkillSet()) {
                if(qualifications.getDesignation().equals(qualification)){
                    retour = true;
                    return retour;
                }
            }
        }
        return retour;
    }


    public boolean isEmployeeBlocked(long id, LocalDate start_date, LocalDate end_date, ProjectService project_service){
        boolean retour = false; // employee is not blocked
        List<Project> all_projects = project_service.readAll();
        for (Project single_project:
             all_projects) {
            System.out.println("projekt_id: " + single_project.getId());
            for (EmployeeQualification employee_obj:
                 single_project.getContributingEmployees()) {
                if(employee_obj.getEmployeeId() == id){
                    System.out.println("employee_id: " + employee_obj.getEmployeeId());
                    // the employee is in this project
                    if(!(start_date.isAfter(single_project.getPlannedEndDate()))){
                        // start is not after project end
                        if(!(end_date.isBefore(single_project.getStartDate()))){
                            // end is not before project start
                            retour = true; // employee is blocked
                            return retour;
                        }
                    }
                }
            }
        }
        return retour;
    }
}
