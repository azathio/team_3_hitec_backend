package de.szut.lf8_project.service;

import de.szut.lf8_project.model.EmployeeQualification;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.model.dto.AddProjectDTO;
import de.szut.lf8_project.model.dto.EmployeeQualiProjectDTO;
import de.szut.lf8_project.model.dto.GetProjectDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MappingService {

    public List<EmployeeQualiProjectDTO> mapEmpQualToEmpQualDTO(List<EmployeeQualification> input){
        List<EmployeeQualiProjectDTO> emp = new ArrayList<>();
        for (EmployeeQualification employeeQualification : input) {
            EmployeeQualiProjectDTO tempEmpQuali = new EmployeeQualiProjectDTO();
            tempEmpQuali.setId(employeeQualification.getEmployeeId());
            tempEmpQuali.setQualification(employeeQualification.getQualification());
            emp.add(tempEmpQuali);
        }
        return emp;
    }

    public List<EmployeeQualification> mapEmpQualDTOToEmpQual(List<EmployeeQualiProjectDTO> input){
        List<EmployeeQualification> emp = new ArrayList<>();
        for (EmployeeQualiProjectDTO employeeQualificationDTO : input) {
            EmployeeQualification tempEmpQuali = this.mapSingleEmpQualDTOToEmpQual(employeeQualificationDTO);
            emp.add(tempEmpQuali);
        }
        return emp;
    }

    public EmployeeQualification mapSingleEmpQualDTOToEmpQual(EmployeeQualiProjectDTO input){
        EmployeeQualification employeeQualification = new EmployeeQualification();
        employeeQualification.setEmployeeId(input.getId());
        employeeQualification.setQualification(input.getQualification());
        return employeeQualification;
    }

    public GetProjectDTO mapProjectToGetProjectDTO(Project project){
        GetProjectDTO pro = new GetProjectDTO();

        pro.setProjectId(project.getId());
        pro.setProjectName(project.getProjectName());
        pro.setLeadEmployeeId(project.getLeadEmployeeId());
        pro.setCustomerId(project.getCustomerId());
        pro.setContactPersonName(project.getContactPersonName());
        pro.setProjectGoal(project.getProjectGoal());
        pro.setStartDate(project.getStartDate());
        pro.setPlannedEndDate(project.getPlannedEndDate());
        pro.setActualEndDate(project.getActualEndDate());
        pro.setContributingEmployees(this.mapEmpQualToEmpQualDTO(project.getContributingEmployees()));

        return pro;
    }

    public Project mapAddProjectDtoToProject(AddProjectDTO projectDto){
        Project pro = new Project();

        pro.setProjectName(projectDto.getProjectName());
        pro.setLeadEmployeeId(projectDto.getLeadEmployeeId());
        pro.setCustomerId(projectDto.getCustomerId());
        pro.setContactPersonName(projectDto.getContactPersonName());
        pro.setProjectGoal(projectDto.getProjectGoal());
        pro.setStartDate(projectDto.getStartDate());
        pro.setPlannedEndDate(projectDto.getPlannedEndDate());
        pro.setActualEndDate(projectDto.getActualEndDate());
        pro.setContributingEmployees(this.mapEmpQualDTOToEmpQual(projectDto.getContributingEmployees()));

        return pro;
    }
}
