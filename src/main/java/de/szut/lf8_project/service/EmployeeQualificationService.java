package de.szut.lf8_project.service;

import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.model.EmployeeQualification;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.repository.EmployeeQualificationRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service

public class EmployeeQualificationService {

    private final EmployeeQualificationRepository repository;

    public EmployeeQualificationService(EmployeeQualificationRepository repository) {
        this.repository = repository;
    }

    public EmployeeQualification create(EmployeeQualification newEmployeeQualificationService) {
        return repository.save(newEmployeeQualificationService);
    }

    public List<Project> readAll(){
        return repository.findAllByOrderByIdDesc();
    }

    public EmployeeQualification readById(Long id){
        Optional<EmployeeQualification> s = repository.findById(id);

        if (s.isEmpty()) {
            throw new ResourceNotFoundException("Project with Id: " + id + " could not found");
        }
        return s.get();
    }

    public void delete(Long id){
        repository.deleteById(id);
    }
}
