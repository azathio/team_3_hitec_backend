package de.szut.lf8_project.controller;

import de.szut.lf8_project.exceptionHandling.DataIncompleteException;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.model.EmployeeQualification;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.model.dto.AddProjectDTO;
import de.szut.lf8_project.model.dto.EmployeeDTO;
import de.szut.lf8_project.model.dto.EmployeeQualiProjectDTO;
import de.szut.lf8_project.model.dto.GetProjectDTO;
import de.szut.lf8_project.service.EmployeeService;
import de.szut.lf8_project.service.MappingService;
import de.szut.lf8_project.service.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/project")
public class ProjectController {
    private final MappingService mappingService;
    private final ProjectService projectService;
    private final EmployeeService employeeService;


    public ProjectController(MappingService mappingService, ProjectService projectService, EmployeeService empService) {
        this.mappingService = mappingService;
        this.projectService = projectService;
        this.employeeService = empService;
    }

    @Operation(summary = "returns all projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project was updated"),
            @ApiResponse(responseCode = "400", description = "project could not be found",
                         content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping
    public ResponseEntity<List<GetProjectDTO>> findAllProjects(@RequestHeader Map<String,String> headers){

        List<Project> all = this.projectService.readAll();
        List<GetProjectDTO> dtoList = new LinkedList<>();
        for (Project project: all) {
            dtoList.add(this.mappingService.mapProjectToGetProjectDTO(project));
        }
        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }

    @Operation(summary = "create a new project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project was updated"),
            @ApiResponse(responseCode = "400", description = "project param is null",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping
    public ResponseEntity<GetProjectDTO> createProject(@RequestHeader Map<String,String> headers, @Valid @RequestBody final AddProjectDTO dto){
        String token = this.getToken(headers);
        Project pDto = this.mappingService.mapAddProjectDtoToProject(dto);
        if(!employeeService.checkIfEmployeeExists(pDto.getLeadEmployeeId(), token)){
            throw new ResourceNotFoundException("Leading employee does not exists!");
        }
        for (EmployeeQualification emp:
             pDto.getContributingEmployees()) {
            if(!employeeService.checkIfEmployeeOwnsQualification(emp.getEmployeeId(), emp.getQualification(), token)){
                throw new ResourceNotFoundException("Employee with the choosen qualification does not exists!");
            }
        }
        this.projectService.create(pDto);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(pDto);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @Operation(summary = "updates a existing project, by providing resending new project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project was updated"),
            @ApiResponse(responseCode = "404", description = "project could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping("/{id}")
    public ResponseEntity<GetProjectDTO> updateProject(@RequestHeader Map<String,String> headers, @Valid @RequestBody final AddProjectDTO dto,
                                                       @PathVariable final Long id){
        String token = this.getToken(headers);
        Project pDto = this.mappingService.mapAddProjectDtoToProject(dto);
        if(!employeeService.checkIfEmployeeExists(pDto.getLeadEmployeeId(), token)){
            throw new ResourceNotFoundException("Leading employee does not exists!");
        }
        for (EmployeeQualification emp:
                pDto.getContributingEmployees()) {
            if(!employeeService.checkIfEmployeeOwnsQualification(emp.getEmployeeId(), emp.getQualification(), token)){
                throw new ResourceNotFoundException("Employee with the choosen qualification does not exists!");
            }
        }
        Project updatedProject = this.projectService.update(pDto, id);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(updatedProject);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @Operation(summary = "updates a existing project, by providing certain fields to update")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project was updated"),
            @ApiResponse(responseCode = "400", description = "supplied field to update does not exist or has wrong type"),
            @ApiResponse(responseCode = "404", description = "project or lead employee could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PatchMapping("/{id}")
    public ResponseEntity<GetProjectDTO> patchProject(@RequestHeader Map<String,String> headers, @Valid @RequestBody final Map<String,Object> patch,
                                                       @PathVariable final Long id){
        String token = this.getToken(headers);

        if(patch.containsKey("leadEmployeeId"))
            if(!employeeService.checkIfEmployeeExists(Long.parseLong(patch.get("leadEmployeeId").toString()), token))
                throw new ResourceNotFoundException("Leading employee does not exists!");

        if (patch.containsKey("contributingEmployees"))
            throw new DataIncompleteException("Use the proper Endpoints meant for managing contributors" +
                    " e.g. /removeContributor/... or /addEmployeeToProject/...");

        Project updatedProject = this.projectService.patch(patch, id);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(updatedProject);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @Operation(summary = "removes contributing employee from a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "contributer was removed from the project"),
            @ApiResponse(responseCode = "400", description = "employee id or project id could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping("/removeContributor/{projectId}/{employeeId}")
    public ResponseEntity<String> removeContributorFromProject(@Valid @PathVariable("projectId") final Long projectId,
                                                      @PathVariable("employeeId") final Long employeeId) {
        boolean employeeIsContributor = this.projectService.checkIfEmployeeIsContributor(projectId, employeeId);

        if(employeeIsContributor){
            boolean listWasChanged = this.projectService.removeContributorFromProject(projectId, employeeId);
            if(listWasChanged){
                return new ResponseEntity<>("", HttpStatus.OK);
            }
            else {
                throw new DataIncompleteException("Could not remove the employee from given project!");
            }
        }
        else {
            throw new ResourceNotFoundException("Employee is not listed as contributer!");
        }
    }

    @Operation(summary = "get all contributing employees from a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "get a json with all contributing employees from the project"),
            @ApiResponse(responseCode = "400", description = "project id could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/contributors/{projectId}")
    public ResponseEntity<List<EmployeeDTO>> getContributorFromProject(@RequestHeader Map<String,String> headers,
                                                                          @Valid @PathVariable("projectId") final Long projectId)
    {
        String token = this.getToken(headers);
        List<EmployeeDTO> resultList = new LinkedList<>();
        Project selectedProject = this.projectService.readById(projectId);
        for (EmployeeQualification employeeId : selectedProject.getContributingEmployees()) {
            EmployeeDTO newEmployee = this.employeeService.getEmployeeById(employeeId.getEmployeeId(), token);
            resultList.add(newEmployee);
        }

        return new ResponseEntity<>(resultList, HttpStatus.OK);
    }

    @Operation(summary = "get all projects where the employee is a contributor")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "get a json with all projects where the employee is a contributor."),
            @ApiResponse(responseCode = "400", description = "project id could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/employee/{employeeId}")
    public ResponseEntity<List<GetProjectDTO>> getProjectsFromEmployee(@RequestHeader Map<String,String> headers,
                                                                          @Valid @PathVariable("employeeId") final Long employeeId)
    {
        String token = this.getToken(headers);
        if(this.employeeService.checkIfEmployeeExists(employeeId, token)) {

            List<GetProjectDTO> resultList = new LinkedList<>();
            for (Project project : this.projectService.readAll()) {
                if (this.projectService.checkIfEmployeeIsContributor(project.getId(), employeeId) || project.getLeadEmployeeId() == employeeId) {
                    resultList.add(this.mappingService.mapProjectToGetProjectDTO(project));
                }
            }

            return new ResponseEntity<>(resultList, HttpStatus.OK);
        }
        else {
            throw new ResourceNotFoundException("The employee with the id: " + employeeId + " doesn't exist!");
        }
    }

    @Operation(summary = "get project from database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project with the specified id exists and got returned"),
            @ApiResponse(responseCode = "400", description = "project id could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<GetProjectDTO> findSingleProjectById(@RequestHeader Map<String,String> headers, @PathVariable final Long id){
        Project project = this.projectService.readById(id);
        GetProjectDTO projectDTO = this.mappingService.mapProjectToGetProjectDTO(project);
        return new ResponseEntity<>(projectDTO, HttpStatus.OK);
    }

    @Operation(summary = "delete a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project was deleted"),
            @ApiResponse(responseCode = "404", description = "project could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProject(@RequestHeader Map<String,String> headers, @PathVariable final Long id){
        this.projectService.delete(id);
        return new ResponseEntity<>("", HttpStatus.OK);
    }


    @Operation(summary = "add employee with qualification")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "employee was added"),
            @ApiResponse(responseCode = "404", description = "employee with qualification could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping("/addEmployeeToProject/{projectid}")
    public ResponseEntity<EmployeeQualification> addEmployeeWithQualification(@RequestHeader Map<String,String> headers,
                                                               @PathVariable final Long projectid,
                                                               @Valid @RequestBody final EmployeeQualiProjectDTO employee){
        String token = this.getToken(headers);
        Project project = this.projectService.readById(projectid);
        EmployeeQualification emp = this.mappingService.mapSingleEmpQualDTOToEmpQual(employee);
        if(employeeService.checkIfEmployeeOwnsQualification(emp.getEmployeeId(), emp.getQualification(), token)){
            if(!employeeService.isEmployeeBlocked(emp.getEmployeeId(), project.getStartDate(), project.getPlannedEndDate(), projectService)){
                projectService.addContributingEmployee(projectid, emp);
                return new ResponseEntity<>(emp, HttpStatus.OK);
            }
        }
        throw new ResourceNotFoundException("Employee with the choosen qualification does not exists!");
    }

    public String getToken(Map<String,String> headers) {

        String bearerToken = headers.get("authorization").replace("Bearer ","");

        if (bearerToken.isEmpty())
            throw new DataIncompleteException("No bearer token could be found");

        return bearerToken;
    }
}
