package de.szut.lf8_project.templates;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.exceptionHandling.UnauthorizedException;
import de.szut.lf8_project.model.dto.EmployeeDTO;
import de.szut.lf8_project.model.dto.EmployeeQualificationsDTO;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class EmployeeAccessTemplate {

    private String key;
    private final String baseUrl = "https://employee.szut.dev/";
    private RestTemplate restTemplate = new RestTemplate();

    public EmployeeAccessTemplate(){
        key = null;
    }

    private HttpEntity getHttpEntity(String token){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(token);
        HttpEntity request = new HttpEntity(headers);
        return request;
    }

    public EmployeeDTO getEmployeeByUrlAndId(String input_url, String token){
        try {
            String url = baseUrl + input_url;
            HttpEntity request = this.getHttpEntity(token);
            ResponseEntity<EmployeeDTO> response = this.restTemplate.exchange(url, HttpMethod.GET, request, EmployeeDTO.class, 1);
            if (response.getStatusCode() == HttpStatus.OK)
                return response.getBody();
        }
        catch (HttpClientErrorException.Unauthorized e) {
            throw new UnauthorizedException();
        }
        catch (HttpClientErrorException.NotFound e) {
            throw new ResourceNotFoundException("Employee was not found!");
        }

        return null;
    }

    public EmployeeQualificationsDTO getEmployeeWithQualificationsByUrlAndId(String input_url, String token){
        String url = baseUrl + input_url;
        HttpEntity request = this.getHttpEntity(token);
        // use `exchange` method for HTTP call
        try {
            ResponseEntity<EmployeeQualificationsDTO> response = this.restTemplate.exchange(url, HttpMethod.GET, request, EmployeeQualificationsDTO.class, 1);
            if(response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            }
        }
        catch (HttpClientErrorException.NotFound exception){
            throw new ResourceNotFoundException("Employee could not be found");
        }
        catch (HttpClientErrorException.Unauthorized exception){
            throw new UnauthorizedException("You are unauthorized to access the service");
        }
        return null;
    }
}
