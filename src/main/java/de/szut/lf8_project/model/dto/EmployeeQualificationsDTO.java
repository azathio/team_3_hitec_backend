package de.szut.lf8_project.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class EmployeeQualificationsDTO {
    @Id
    @NotNull(message = "id name can't be null")
    private long id;

    @NotBlank(message = "surname can't be blank")
    private String surname;

    @NotBlank(message = "firstname can't be blank")
    private String firstname;

    @ManyToMany
    private List<QualificationsDTO> skillSet;
}
