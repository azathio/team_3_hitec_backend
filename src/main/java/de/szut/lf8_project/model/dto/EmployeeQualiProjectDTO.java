package de.szut.lf8_project.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity

public class EmployeeQualiProjectDTO {
    @Id
    @NotNull
    private long id;

    @NotBlank
    private String qualification;
}
