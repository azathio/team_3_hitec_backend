package de.szut.lf8_project.model.dto;

import de.szut.lf8_project.model.EmployeeQualification;
import lombok.Data;

import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

@Data
public class GetProjectDTO {
    private long projectId;
    private String projectName;
    private long leadEmployeeId;
    private long customerId;
    private String contactPersonName;
    private String projectGoal;
    private LocalDate startDate;
    private LocalDate plannedEndDate;
    private LocalDate actualEndDate;
    private List<EmployeeQualiProjectDTO> contributingEmployees;
}
