package de.szut.lf8_project.model.dto;

import de.szut.lf8_project.model.EmployeeQualification;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
public class AddProjectDTO {
    @NotBlank(message = "Project name can't be blank")
    private String projectName;

    @NotNull(message = "leadEmployeeId can't be null")
    private long leadEmployeeId;

    @NotNull(message = "customerId can't be null")
    private long customerId;

    @NotBlank(message = "contactPersonName can't be blank")
    private String contactPersonName;

    @NotBlank(message = "contactPersonName can't be blank")
    private String projectGoal;

    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @Column(name = "planned_end_date", nullable = false)
    private LocalDate plannedEndDate;

    @Column(name = "actual_end_date")
    private LocalDate actualEndDate;

    @OneToMany
    private List<EmployeeQualiProjectDTO> contributingEmployees;
}
