package de.szut.lf8_project.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class EmployeeDTO {

    @Id
    @NotNull(message = "id can't be null")
    private long id;

    @NotBlank(message = "lastname can't be blank")
    private String lastName;

    @NotBlank(message = "firstname can't be blank")
    private String firstName;

    @NotBlank(message = "street can't be blank")
    private String street;

    @NotBlank(message = "postcode can't be blank")
    private String postcode;

    @NotBlank(message = "city can't be blank")
    private String city;

    @NotBlank(message = "phone can't be blank")
    private String phone;
}
