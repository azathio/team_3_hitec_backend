package de.szut.lf8_project.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name="project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Project name is mandatory")
    private String projectName;

    @NotNull
    private long leadEmployeeId;

    @NotNull
    private long customerId;

    @NotBlank
    private String contactPersonName;

    @NotBlank
    private String projectGoal;

    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @Column(name = "planned_end_date", nullable = false)
    private LocalDate plannedEndDate;

    @Column(name = "actual_end_date")
    private LocalDate actualEndDate;

    //@OneToMany(mappedBy = "id")
    @OneToMany
    private List<EmployeeQualification> contributingEmployees;
}
