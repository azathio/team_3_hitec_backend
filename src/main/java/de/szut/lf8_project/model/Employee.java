package de.szut.lf8_project.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity

public class Employee {
    @Id
    @NotNull
    private long employeeId;

    @NotBlank
    private String lastName;

    @NotBlank
    private String firstName;

    @NotBlank
    private String street;

    @NotBlank
    private String postcode;

    @NotBlank
    private String city;

    @NotBlank
    private String phone;

}
